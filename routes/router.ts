import { Router, Request, Response } from 'express';

export const router = Router();

router.get('/messages', (req: Request, res: Response) => {
  res.json({
    ok: true,
    message: 'GET - All good'
  })
})

router.post('/messages', (req: Request, res: Response) => {

  const body = req.body;
  const from = req.body.from;
  console.log(req.body)

  res.json({
    ok: true,
    message: 'POST - All good',
    body,
    from
  })
});

router.post('/messages/:id', (req: Request, res: Response) => {

  const body = req.body.body;
  const from = req.body.from;
  const id = req.params.id;

  res.json({
    ok: true,
    message: 'POST - All good',
    body,
    from,
    id
  })
})

